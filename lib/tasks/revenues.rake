namespace :revenues do
  desc 'prints revenues for specified months'

  # rake revenues:print_examples
  task print_examples: [:environment] do
    %w(2013-01 2013-06 2014-03 2014-09 2015-07).each do |date|
      puts Reservation.to_s(date)
    end
  end

  # examples of usage
  # rake revenues:print_revenue_for['yyyy-mm']
  # rake revenues:print_revenue_for['2000-10']
  # rake revenues:print_revenue_for['2018-08']
  task :print_revenue_for,[:date] => [:environment] do |t, args|
    puts Reservation.to_s(args[:date])
  end
end