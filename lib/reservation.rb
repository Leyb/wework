require 'csv'

class Reservation

  class << self

    def read_file(file = 'data.csv')
      csv_file = File.read(file)
      CSV.parse(csv_file, headers: true)
    end

    def date_in_range?(start, finish, date_to_check)
      month = date_to_check[-2..-1] #get month from str
      year = date_to_check[0..3] #get year from str
      finish = Date.today unless finish.present?
      range = start.to_date..finish.to_date

      range.cover?("01-#{month}-#{year}".to_date) # 'mm-yyyy' format doesnt exist in ruby
    end

    def get_lines_in_month(date_to_check)
      text = read_file
      text.select do |record|
        start = "#{record[2]}".strip
        finishes = "#{record[3]}".strip
        date_in_range?(start, finishes, date_to_check)
      end
    end

    def calculate_revenue(date_to_check)
      prices = get_lines_in_month(date_to_check)
      sum_lines(prices, 1)
    end

    def get_offices
      text = read_file
      sum_lines(text, 0)
    end

    def occupied_offices(date_to_check)
      lines = get_lines_in_month(date_to_check)
      sum_lines(lines, 0)
    end

    def get_available_offices(date_to_check)
      occupied = occupied_offices(date_to_check)
      get_offices - occupied
    end

    def to_s(date_to_check = '02-2018')
      revenue = calculate_revenue(date_to_check)
      available_offices = get_available_offices(date_to_check)
      " #{date_to_check}: expected revenue: $#{revenue}, expected total capacity of the unreserved offices: #{available_offices}"
    end

    def sum_lines(arr, key) # TODO: is ugly to add method that requires arr.. should be more OO
      arr.map{|line| line[key].strip.to_i}.reduce(0, :+)  # TODO: would be clearer with the key as string, maybe enum
    end

  end

end