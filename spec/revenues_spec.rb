require 'spec_helper'
require './lib/reservation' # FIXME: this shouldn't be necessary

describe 'revenues calculations' do

  it 'should  check values w/o reservations ' do
    example1 = Reservation.to_s('2000-10')
    expect(example1).to eq(' 2000-10: expected revenue: $0, expected total capacity of the unreserved offices: 266' )
  end

  it 'should  check values relevant for this month ' do
    this_month = Time.current.strftime("%Y-%m")
    example2 = Reservation.to_s(this_month)
    expect(example2).to eq(" #{this_month}: expected revenue: $75500, expected total capacity of the unreserved offices: 137" )
  end

  it 'should  check values relevant for a year back ' do
    this_month = 1.year.ago.strftime("%Y-%m")
    example3 = Reservation.to_s(this_month)
    expect(example3).to eq(" #{this_month}: expected revenue: $75500, expected total capacity of the unreserved offices: 137" )
  end
end